#CHECK kVM
sudo apt install cpu-checker && sudo kvm-ok

#INSTALL LIBVIRT KVM
sudo apt install libvirt-clients libvirt-daemon-system qemu-kvm && sudo usermod -a -G libvirt $(whoami)  && newgrp libvirt
sudo virt-host-validate

#INSTALL KUBERNETS COMMAND LINE TOOL (kubectl)
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version

#INSTALL MINIKUBE
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 
chmod +x minikube
sudo cp minikube /usr/local/bin && rm minikube
minikube version
minikube start --vm-driver kvm2
minikube config set vm-driver kvm2
minikube dashboard

kubectl create deployment hello-node --image=gcr.io/hello-minikube-zero-install/hello-node
kubectl get deployments
kubectl get pods
kubectl get events
kubectl config view

kubectl expose deployment hello-node --type=LoadBalancer --port=8080
minikube service hello-node

minikube addons list
minikube addons enable heapster
kubectl get pod,svc -n kube-system


#DEPLOYMENTS

kubectl apply -f https://k8s.io/examples/controllers/nginx-deployment.yaml
kubectl get deployments
kubectl rollout status deployment.v1.apps/nginx-deployment
kubectl get rs
kubectl get pods --show-labels

Update deployment
kubectl --record deployment.apps/nginx-deployment set image deployment.v1.apps/nginx-deployment nginx=nginx:1.9.1
kubectl edit deployment.v1.apps/nginx-deployment
kubectl rollout status deployment.v1.apps/nginx-deployment
kubectl describe deployments


kubectl rollout history deployment.v1.apps/nginx-deployment
kubectl rollout history deployment.v1.apps/nginx-deployment --revision=2
kubectl rollout undo deployment.v1.apps/nginx-deployment
kubectl rollout undo deployment.v1.apps/nginx-deployment --to-revision=2

kubectl get deployment nginx-deployment
kubectl describe deployment nginx-deployment

#SCALING
kubectl scale deployment.v1.apps/nginx-deployment --replicas=10

kubectl autoscale deployment.v1.apps/nginx-deployment --min=10 --max=15 --cpu-percent=80
kubectl get deploy